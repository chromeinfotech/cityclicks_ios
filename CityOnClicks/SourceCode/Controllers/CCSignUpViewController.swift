//
//  CCSignUpViewController.swift
//  CityOnClicks
//
//  Created by Yash Agarwal on 13/11/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import UIKit

class CCSignUpViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nameButton: UIButton!
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailButton: UIButton!
    
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var numberButton: UIButton!
    
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordButton: UIButton!
    
    @IBOutlet weak var dobView: UIView!
    @IBOutlet weak var dobTextField: CCTextField!
    @IBOutlet weak var dodButton: UIButton!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var cityButton: UIButton!
    @IBOutlet weak var cityTextField: CCTextField!
    
    @IBOutlet weak var postalCodeView: UIView!
    @IBOutlet weak var postalCodeButton: UIButton!
    @IBOutlet weak var postalCodeTextField: UITextField!
    
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var referralCodeButton: UIButton!
    
    // MARK: - Properties
    
    var datePicker = UIDatePicker()
    
    // MARK: - Override Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
    
    // MARK: - Actions
    
    @IBAction func genderButtonTapped(_ sender: UIButton){
    }
    
    @IBAction func signUpButton(_ sender: UIButton) {
    }
    
    @IBAction func referralCodeButton(_ sender: UIButton) {
    }
    
    // MARK: - Selector
    
    /**
     Picking date from date picker
     
     - Parameter sender: UIDatePicker
     */
    
    @objc func datePicker(_ sender: UIDatePicker){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constant.date_format_1
        dobTextField.text = dateFormatter.string(from: datePicker.date)
    }
}

// MARK: - Private Method

extension CCSignUpViewController{
    /**
     Customize Sign Up UI
     */
    private func customizeUI(){
        
        nameTextField.placeholder = Constant.CCText.Placeholder.fullName
        emailTextField.placeholder = Constant.CCText.Placeholder.emailAdd
        numberTextField.placeholder = Constant.CCText.Placeholder.mobileNum
        passwordTextField.placeholder = Constant.CCText.Placeholder.password
        dobTextField.placeholder = Constant.CCText.Placeholder.dob
        cityTextField.placeholder = Constant.CCText.Placeholder.city
        postalCodeTextField.placeholder = Constant.CCText.Placeholder.postalCode
        
        addressLabel.text = Constant.CCText.Label.address
        genderLabel.text = Constant.CCText.Label.gender
        
        maleButton.setTitle(Constant.CCText.Button.male, for: .normal)
        femaleButton.setTitle(Constant.CCText.Button.female, for: .normal)
        signUpButton.setTitle(Constant.CCText.Button.signUp, for: .normal)
        referralCodeButton.setTitle(Constant.CCText.Button.referralCode, for: .normal)
        
        signUpButton.bottomShadow(5)
        profileImageView.displayPictureLayoutCircle()
        
        let toolBar = YAToolbar.instantiateToolBar()
        toolBar.setToolBar(textFieldsAndViews: [numberTextField, passwordTextField, dobTextField, cityTextField, postalCodeTextField])
        
        datePicker.maximumDate = Date()
        datePicker.datePickerMode = .date
        dobTextField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(datePicker(_:)), for: .valueChanged)
    }
}

// MARK: - UITextFieldDelegate

extension CCSignUpViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField {
        case nameTextField:
            return emailTextField.becomeFirstResponder()
        case emailTextField:
            return numberTextField.becomeFirstResponder()
        case numberTextField:
            return passwordTextField.becomeFirstResponder()
        case passwordTextField:
            return dobTextField.becomeFirstResponder()
        case dobTextField:
            return cityTextField.becomeFirstResponder()
        case cityTextField:
            return postalCodeTextField.becomeFirstResponder()
        default:
            return textField.resignFirstResponder()
        }
    }
}

// MARK: - Static Methods

extension CCSignUpViewController{
    /**
     Instantiate CCSignUpViewController from storyboard Main
     
     - Returns: CCSignUpViewController
     */
    static func instantiateFromStoryboard() -> CCSignUpViewController{
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let signupController = storyboard.instantiateViewController(withIdentifier: "CCSignUpViewController")
        return signupController as! CCSignUpViewController
    }
}
