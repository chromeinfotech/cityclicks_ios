//
//  CCLoginViewController.swift
//  CityOnClicks
//
//  Created by Yash Agarwal on 13/11/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import UIKit

class CCLoginViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var passwordButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var getAccessButton: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    
    // MARK: - Override Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
    
    // MARK: - Actions
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotPasswordButton(_ sender: UIButton) {
        let forgotPasswordView = CCForgotPassword.loadXib()
        forgotPasswordView.showForgotPassView(in: self.view) { (value) in
            if value{
                Utility.showMessage("Send")
            }
        }
    }
    
    @IBAction func getAccessButton(_ sender: UIButton) {
        if isAllFieldValidate(){
            webRequestForSignIn()
        }
    }
    
    @IBAction func facebookButton(_ sender: UIButton) {
    }
    
    @IBAction func twitterButton(_ sender: UIButton) {
    }
}

// MARK: - Private Methods

extension CCLoginViewController{
    
    /**
     Customize Login UI
     */
    private func customizeUI(){
        
        loginLabel.text = Constant.CCText.Heading.login
        emailTextField.placeholder = Constant.CCText.Placeholder.emailMob
        passwordTextField.placeholder = Constant.CCText.Placeholder.password
        forgotPasswordButton.setTitle("\(Constant.CCText.Button.forgotPass) ?", for: .normal)
        getAccessButton.setTitle(Constant.CCText.Button.getAccess, for: .normal)
        orLabel.text = Constant.CCText.Label.or
        facebookButton.setTitle(Constant.CCText.Button.facebook, for: .normal)
        twitterButton.setTitle(Constant.CCText.Button.twitter, for: .normal)
        
        getAccessButton.bottomShadow(5)
    }
    
    /**
     To check is all field valid or not
     
     - Returns: Bool value
     */
    private func isAllFieldValidate() -> Bool{
        var isValid:Bool = false
        
        if Validator.isTextFieldEmpty(emailTextField){
            Utility.showMessage(Constant.CCText.Message.validEmailNo)
        }else if !Validator.isValidPassword(passwordTextField.text){
            Utility.showMessage(Constant.CCText.Message.validPass)
        }else{
            isValid = true
        }
        
        return isValid
    }
    
    /**
     Change the view and icon color of selected text field.
     
     - Parameter textField: Selected text field
     */
    private func textFieldSelected(_ textField:UITextField){
        
        let viewArray:[UIView] = [emailView,passwordView]
        let buttonArray:[UIButton] = [emailButton,passwordButton]
        
        for view in viewArray{
            view.backgroundColor = UIColor.lightGray
        }
        
        for button in buttonArray{
            button.isSelected = false
        }
        
        switch textField {
        case emailTextField:
            emailButton.isSelected = true
            emailView.backgroundColor = UIColor.themeColor()
        case passwordTextField:
            passwordButton.isSelected = true
            passwordView.backgroundColor = UIColor.themeColor()
        default:
            return
        }
    }
    
    // MARK: - API_Methods
    
    private func webRequestForSignIn(){
        
    }
    
}

// MARK: - UITextFieldDelegate

extension CCLoginViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        switch textField{
        case emailTextField:
            return passwordTextField.becomeFirstResponder()
        default:
            return textField.resignFirstResponder()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textFieldSelected(textField)
    }
}

// MARK: - Class Methods

extension CCLoginViewController{
    
    /**
     Instantiate Login Controller from Main storybaord.
     
     - Returns: CCLoginViewController
     */
    class func instantiateFromStoryboard() -> CCLoginViewController{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginController = storyboard.instantiateViewController(withIdentifier: "CCLoginViewController")
        return loginController as! CCLoginViewController
    }
}
