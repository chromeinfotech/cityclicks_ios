//
//  CCForgotPassword.swift
//  CityOnClicks
//
//  Created by Yash Agarwal on 15/11/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import UIKit

class CCForgotPassword: UIView {

    // MARK: - Outlets
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var forgotPasswordLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    // MARK: - Properties
    
    var completionHandler:((Bool)->Void)?
    
    // MARK: - Override Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        customizeUI()
    }
    
    // MARK: - Actions
    
    @IBAction func sendButton(_ sender: UIButton) {
        if isAllFieldValid(){
            completionHandler!(true)
            self.remove()
        }
    }
    
    // MARK: - Selector
    
    @objc func dismissView(_ tapGesture: UITapGestureRecognizer){
        self.remove()
    }
    
    // MARK: - Internal Method
    
    /**
     Present forgot password view on screen
     */
    internal func showForgotPassView(in superView: UIView, complition: @escaping((Bool)->Void)){
        completionHandler = complition
        superView.addSubview(self)
        self.show()
    }
}

// MARK: - Private Methods

extension CCForgotPassword{
    
    /**
     Forgot Password customize UI
     */
    private func customizeUI(){
        
        containerView.bottomShadow(10)
        sendButton.bottomShadow(5)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissView(_:)))
        dismissView.addGestureRecognizer(tapGesture)
        
        emailTextField.delegate = self
        
        emailTextField.becomeFirstResponder()
        emailTextField.placeholder = Constant.CCText.Placeholder.emailAdd
        forgotPasswordLabel.text = Constant.CCText.Button.forgotPass
        messageLabel.text = Constant.CCText.Message.forgotPassMsg
        sendButton.setTitle(Constant.CCText.Button.send, for: .normal)
    }
    
    /**
     Show forgot password view on screen
     */
    private func show(){
        popUpView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        popUpView.alpha     = 0;
        
        UIView.animate(withDuration: 0.3) {
            self.popUpView.transform = CGAffineTransform.identity
            self.popUpView.alpha     = 1;
        }
    }
    
    /**
     Remove forgot password view on screen
     */
    private func remove(){
        UIView.animate(withDuration: 0.2, animations: {
            self.popUpView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            self.popUpView.alpha     = 0;
        }) { (Bool) in
            self.removeFromSuperview()
        }
    }
    
    /**
     Valid all fields
     
     - Returns: Bool value
     */
    private func isAllFieldValid() -> Bool{
        var isValid = false
        
        if Validator.isTextFieldEmpty(emailTextField){
            Utility.showMessage(Constant.CCText.Message.validEmailNo)
        }else{
            isValid = true
        }
        return isValid
    }
}

// MARK: - UITextFieldDelegate

extension CCForgotPassword: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

// MARK: - Static Method

extension CCForgotPassword{

    /**
     Load forgot password xib
     
     - Returns: CCForgotPassword
     */
    static func loadXib() -> CCForgotPassword{
        
        let forgotPasswordXib = Bundle.main.loadNibNamed("CCForgotPassword", owner: nil, options: nil)?.first as! CCForgotPassword
        forgotPasswordXib.frame = UIScreen.main.bounds
        return forgotPasswordXib
    }
}
