//
//  FacebookHelper.swift
//  GetIn
//
//  Created by Yash Agarwal on 07/06/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class FacebookHelper: NSObject {
    
    var dict:Dictionary<String,Any> = [:]
    var finishedClouser:((Dictionary<String ,Any>) -> Void)?
    var failedClouser:((String) -> Void)?
    
    func loginUserOn(viewController:UIViewController , finished:@escaping (Dictionary<String ,Any>) -> Void , failed:@escaping (String) -> Void) ->  Void {
        finishedClouser = finished
        failedClouser  = failed
        
        let fbLoginManager:FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: viewController) { (result, error) in
            
            guard let result = result else {
                print("error in login")
                fbLoginManager.logOut()
                return
            }
            
            if let fbloginresult = result as FBSDKLoginManagerLoginResult? {
                
                if fbloginresult.isCancelled {
                    self.failedClouser!("Error in Login")
                    fbLoginManager.logOut()
                    return
                    
                }
                else {
                    
                    guard fbloginresult.grantedPermissions.contains("email") else {
                        self.failedClouser!("Error in Login")
                        fbLoginManager.logOut()
                        return
                    }
                    
                    
                    self.fetchUserData()
                }
            }
        }
    }
    
    
    //Get User Data
    func fetchUserData() -> Void {
        guard FBSDKAccessToken.current() != nil else {
            self.failedClouser!("Error in Login")
            return
        }
        let  request:FBSDKGraphRequest   = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large),email, birthday"])
        request.start(completionHandler: { (connection, result, error) -> Void in
            
            guard let result = result else {
                
                self.failedClouser!("Error in Login")
                return
            }
            self.finishedClouser!(result as! Dictionary<String, Any>)
        })
    }
}
