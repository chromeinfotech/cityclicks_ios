//
//  YATwitterHandler.swift
//  Pactices
//
//  Created by Yash Agarwal on 30/10/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import UIKit

class YATwitterHandler:NSObject{
    
    // MARK: - Properties
    
    var userData = [String:Any]()
    var completionClouser:(([String:Any])->Void)?
    var errorClouser:((String)->Void)?
    
    /**
     Login with twitter
     
     TWTRUser properties:
     - userID
     - name
     - screenName
     - isVerified
     - isProtected
     - profileImageURL
     - profileImageMiniURL
     - profileImageLargeURL
     - formattedScreenName
     - profileURL
     
     - Parameter loginCompletion: Closure Contains the user data in dictionary form with key email(String) & user(TWTRUser)
     - Parameter loginError: Closure Conatins the error while login
     */
   func loginWithTwitter(loginCompletion:@escaping ([String: Any])->Void, loginError:@escaping (String)->Void){

        completionClouser = loginCompletion
        errorClouser = loginError
    
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            /**
             Session properties:
             
             - authToken
             - authTokenSecret
             - userName
             - userID
             */
            if let loginSession = session{
                self.fetchUserEmail(for: loginSession.userID)
                print("signed in as \(String(describing: (loginSession.userName)))")
            } else {
                loginError((error?.localizedDescription)!)
                print("login_error: \(String(describing: error?.localizedDescription))")
            }
        })
    }
    
    /**
     Fetch user email address
     
     - Parameter userID: User id of the login user
     */
    
    func fetchUserEmail(for userId:String?){
        guard let userID = userId else {return}
        
        let client = TWTRAPIClient.withCurrentUser()
        
        client.requestEmail { (email, error) in
            if let userEmail = email{
                self.userData["email"] = userEmail
                self.fetchUserData(for: userID,from: client)
            }else{
                self.errorClouser!(error as! String)
                print("email_error",error?.localizedDescription ?? "")
            }
        }
    }
    
    /**
     Fetch user data

     - Parameter userID: User id of the login user
     - Parameter client: TWTRAPIClient
     */
    func fetchUserData(for userId:String?,from loginClient: TWTRAPIClient?){
        guard let userID = userId,let client = loginClient  else {return}
        
        client.loadUser(withID: userID) { (user, error) in
            if let userData = user{
                self.userData["user"] = userData
                self.completionClouser!(self.userData)
            }else{
                self.errorClouser!(error as! String)
                print("load_user_error",error?.localizedDescription ?? "")
            }
        }
    }
}
