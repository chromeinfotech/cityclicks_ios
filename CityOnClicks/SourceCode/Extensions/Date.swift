//
//  Date.swift
//  CityOnClicks
//
//  Created by Yash Agarwal on 13/11/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import Foundation

extension Date{
    
    static func getDateFromString(from date:String, inFormat format:String) -> Date?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.date(from: date)
    }
    
    static func getStringFromDate(from date:Date,inFormat format:String) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: date)
    }
    
    /**
     Takes the UTC date string and prepare the Date .
     - Parameter string: String representing the date in UTC.
     - Parameter dateFormat: Format of the UTC date string.
     - Returns: Returns Date object with UTC timezone.
     */
    static func getUTCDateFromString(_ string:String, withFormat dateFormat:String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = dateFormatter.date(from: string)!
        return date
    }
}
