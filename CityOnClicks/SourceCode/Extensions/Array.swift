//
//  Array.swift
//  CityOnClicks
//
//  Created by Yash Agarwal on 13/11/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import Foundation

extension Array{
    
    func getJsonForArray() -> String{
        //Preparing json of array
        let jsonValue: Data? = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
        let jsonString = String(data: jsonValue ?? Data(), encoding: .utf8)
        return jsonString!
    }
}
