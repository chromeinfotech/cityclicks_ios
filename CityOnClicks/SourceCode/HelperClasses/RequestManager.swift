//
//  RequestManager.swift
//  MiskTeacher
//
//  Created by Sumit Kumar Gupta on 05/02/18.
//  Copyright © 2018 Sumit Kumar Gupta. All rights reserved.
//

import UIKit
import Alamofire

class RequestManager: NSObject {
    
    /**
     Make a HTTP GET request and returns with either response or failure
     - Parameter url Request URL
     - Parameter parameters Request Parameters
     */
    func requestHTTPGET(url:String, parameters:Dictionary<String,Any>?, finished:@escaping (Dictionary<String, Any>)->Void, failed:@escaping (String)->Void) {
        // HTTP Get request
        
        let request = Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {
                    print(data)
                    finished(data as! Dictionary<String, Any>)
                }
                break
                
            case .failure(_):
                if let error = response.result.error {
                    failed(error.localizedDescription)
                    print(error.localizedDescription)
                }
                break
            }
        }
        //print(request)
    }
    
    /**
     Make a HTTP POST request and returns with either response or failure
     - Parameter url Request URL
     - Parameter parameters Request Parameters
     */
    func requestHTTPPOST(url:String, parameters:Dictionary<String,Any>, finished:@escaping (Dictionary<String, Any>)->Void, failed:@escaping (String)->Void) -> DataRequest {
        // HTTP POST request
        print(parameters)
       
        let request =  Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {
                    print(data)
                    finished(data as! Dictionary<String, Any>)
                }
                break
                
            case .failure(_):
                if let error = response.result.error {
                    failed(error.localizedDescription)
                    print(error.localizedDescription)
                }
                break
            }
        }
        print(request)
        return request
    }
    
    
    /**
     Make a HTTP POST request and returns with either response or failure
     - Parameter url Request URL
     - Parameter parameters Request Parameters
     */
    func requestHTTPPOSTJSON(url:String, parameters:Dictionary<String,Any>, finished:@escaping (Dictionary<String, Any>)->Void, failed:@escaping (String)->Void) -> DataRequest? {
        
        print(parameters)
        let request = Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value {
                    print(data)
                    finished(data as! Dictionary<String, Any>)
                }
            case .failure(_):
                if let error = response.result.error {
                    failed(error.localizedDescription)
                    print(error)
                }
            }
        }
        print(request)
        return request
    }
    
    func uploadFileForData(fileData:Data?, url:String ,parameters:Dictionary<String,Any>,fileKey:String,mimeType:String,finished:@escaping(Dictionary<String,Any>) -> Void , failed:@escaping(String) -> Void) -> Void {
        print(parameters)
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.upload(multipartFormData:{ (multipartFormData) in
            if fileData != nil{
                multipartFormData.append(fileData!, withName: fileKey, fileName:"image.png", mimeType:mimeType)
            }
            for (key, value) in parameters {
                let value = String(describing: value)
                multipartFormData.append(value.data(using: String.Encoding.utf8)! , withName: key)
            }
        }, to: url, method: .post , headers:nil, encodingCompletion: { result in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print(response)
                    let data = response.result.value
                    guard data != nil else {
                        return
                    }
                    finished(data as! Dictionary<String, Any>)
                }
            case .failure(let encodingError):
                print("ER breakROR RESPONSE: \(encodingError)")
            }
        })
    }
    
   
    func uploadMultipleFileForData(fileData:[Data]?, url:String, parameters:Dictionary<String,Any>,fileKey:String,mimeType:String,finished:@escaping(Dictionary<String,Any>)->Void, faied:@escaping(String)->Void) -> Void{
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 120
        
        manager.upload(multipartFormData:{ (multipartFormData) in
            if let imageData = fileData{
                for data in imageData{
                    multipartFormData.append(data, withName: fileKey, fileName: "image.jpg", mimeType: mimeType)
                }
            }
            
            for (key, value) in parameters {
                let value = String(describing: value)
                multipartFormData.append(value.data(using: String.Encoding.utf8)! , withName: key)
            }
        }, to: url, method: .post , headers:nil, encodingCompletion: { result in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    //print(response)
                    let data = response.result.value
                    guard data != nil else {
                        return
                    }
                    finished(data as! Dictionary<String, Any>)
                }
            case .failure(let encodingError):
                print("ER breakROR RESPONSE: \(encodingError)")
            }
        })
        
    }
    
    /**func uploadFile(data: Data, path: String, bucket: String, contentType: String, finished:@escaping(AWSS3TransferUtilityUploadTask) -> Void , failed:@escaping(String) -> Void) -> Void {
        let fileName: String = URL.init(string: path)!.lastPathComponent
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                // Do something e.g. Update a progress bar.
            })
        }
        
        var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                if error == nil{
                    finished(task)
                }else{
                    failed(error!.localizedDescription)
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        
        transferUtility.uploadData(data,
                                   bucket: bucket,
                                   key: fileName,
                                   contentType: contentType,
                                   expression: expression,
                                   completionHandler: completionHandler).continueWith {
                                    (task) -> AnyObject! in
                                    if let error = task.error {
                                        //print("Error: \(error.localizedDescription)")
                                    }
                                    
                                    if let _ = task.result {
                                        // Do something with uploadTask.
                                    }
                                    return nil;
        }
    }*/
}
