//
//  CCSplashViewController.swift
//  CityOnClicks
//
//  Created by Yash Agarwal on 13/11/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import UIKit

class CCSplashViewController: UIViewController {

    // MARK: - Override Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let videoController = CCVideoViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(videoController, animated: true)
    }
}
