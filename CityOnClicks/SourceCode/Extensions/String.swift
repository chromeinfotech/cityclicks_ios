//
//  String.swift
//  CityOnClicks
//
//  Created by Yash Agarwal on 13/11/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import Foundation

extension String{
    
    func toBool() -> Bool?{
        
        switch self.lowercased(){
        case "yes","1","true":
            return true
        case "no","0","false":
            return false
        default:
            return nil
        }
    }
    
    func toJSONObject() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
}
