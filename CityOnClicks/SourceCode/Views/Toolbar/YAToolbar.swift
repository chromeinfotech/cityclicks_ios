//
//  SWToolbarView.swift
//  SportWidget
//
//  Created by Yash Agarwal on 02/04/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import UIKit
/**
 Tag the textFields and textViews starting with 1.
 */
class YAToolbar: UIToolbar {
    
    var presentInputFieldTag:Int?, presentInputFields:Any?
    var textFieldAndViews:[Any]?
    
    
    // MARK: - OUTLETS
    
    @IBOutlet weak var nextButton: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    /// Required initializer for the making the ToolBar default
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sizeToFit()
    }
    
    // MARK: - ACTIONS
    
    @IBAction func doneButton(_ sender: UIBarButtonItem) {
        getActiveTextFieldAndView()
        
        if presentInputFields is UITextField{
            (presentInputFields as! UITextField).resignFirstResponder()
        }else if presentInputFields is UITextView{
            (presentInputFields as! UITextView).resignFirstResponder()
        }
    }
    
    
    @IBAction func nextButton(_ sender: UIBarButtonItem) {
        getActiveTextFieldAndView()
        
        checker: do{
            defer{
                if presentInputFields is UITextField {
                    (presentInputFields as! UITextField).becomeFirstResponder()
                }else if presentInputFields is UITextView{
                    (presentInputFields as! UITextView).becomeFirstResponder()
                }
            }
            
            if presentInputFieldTag! <= textFieldAndViews!.count-1{
                presentInputFieldTag = presentInputFieldTag! + 1
                presentInputFields = textFieldAndViews![presentInputFieldTag!-1]
                
                if (presentInputFields is UITextField || presentInputFields is UITextField) && !(presentInputFields as! UITextField).isEnabled{
                    continue checker
                }
            }
        }
    }
    
    // MARK: -OPEN Methods
    
    open func setToolBar(textFieldsAndViews:[Any]) {
        textFieldAndViews = textFieldsAndViews
        for fields in textFieldAndViews!{
            if fields is UITextView{
                (fields as! UITextView).inputAccessoryView = self
            }else if fields is UITextField{
                (fields as! UITextField).inputAccessoryView = self
            }
        }
    }
    
    // MARK: - PRIVATE Methods
    
    private func getActiveTextFieldAndView(){
        for field in textFieldAndViews!{
            if field is UITextView && (field as! UITextView).isFirstResponder{
                presentInputFields = field
                presentInputFieldTag = (presentInputFields as! UITextView).tag
            }else if field is UITextField && (field as! UITextField).isFirstResponder{
                presentInputFields = field
                presentInputFieldTag = (presentInputFields as! UITextField).tag
            }
        }
    }
    
    class func instantiateToolBar() -> YAToolbar{
        let toolBar = Bundle.main.loadNibNamed(String(describing: self), owner: self, options: nil)?.first as! YAToolbar
        return toolBar
    }

}
