//
//  Utility.swift
//  MiskTeacher
//
//  Created by Sumit Kumar Gupta on 05/02/18.
//  Copyright © 2018 Sumit Kumar Gupta. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    static func showAlert (_ viewController: UIViewController?, title: String?, message: String?, buttonNames:[String], completion:@escaping((Int)->Void)){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for index in 0..<buttonNames.count{
            let name = buttonNames[index]
            let action = UIAlertAction(title: name, style: .default, handler: { (UIAlertAction) in
                completion(index)
                alert.dismiss(animated: true, completion: {
                })
            })
            action.setValue(UIColor.themeColor(), forKey: "titleTextColor")
            alert.addAction(action)
        }
        if viewController == nil{
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }else{
            viewController!.present(alert, animated: true, completion: nil)
        }
    }
    
    static func showActionSheet (_ viewController: UIViewController?, title: String?, message: String?, sourceRect: CGRect, buttonNames:[String], completion:@escaping((Int)->Void)){
        Utility.showActionSheet(viewController, title: title, message: message, destructiveIndex: -1, sourceRect: sourceRect, buttonNames: buttonNames, completion: completion)
    }
    
    static func showActionSheet (_ viewController: UIViewController?, title: String?, message: String?, destructiveIndex: Int, sourceRect: CGRect, buttonNames:[String], completion:@escaping((Int)->Void)){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        for index in 0..<buttonNames.count{
            let name = buttonNames[index]
            
            if index == destructiveIndex{
                let action = UIAlertAction(title: name, style: .destructive, handler: { (UIAlertAction) in
                    completion(index)
                    alert.dismiss(animated: true, completion: {
                    })
                })
                alert.addAction(action)
            }else{
                let action = UIAlertAction(title: name, style: .default, handler: { (UIAlertAction) in
                    completion(index)
                    alert.dismiss(animated: true, completion: {
                    })
                })

                alert.addAction(action)
            }
        }
        let action = UIAlertAction(title: "", style: .cancel, handler: { (UIAlertAction) in
            completion(buttonNames.count)
            alert.dismiss(animated: true, completion: {
            })
        })
        alert.addAction(action)
        
        
        var controller = viewController
        
        if controller == nil{
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            controller = appDelegate?.window?.rootViewController!
        }
        alert.popoverPresentationController?.sourceView = controller?.view;
        alert.popoverPresentationController?.sourceRect = sourceRect;
        
        controller!.present(alert, animated: true, completion: nil)
    }
    
    static func showMessage (_ message: String?){
        Utility.showAlert( nil, title: nil, message: message, buttonNames: [Constant.CCText.Button.ok]) { (index) in
        }
    }
    
    static func showAlertViewController(with title:String?,message:String?,style:UIAlertControllerStyle,complitionHandler:@escaping (Bool)->Void) {
        
        let theAppDelegate = UIApplication.shared.delegate!
        let navigationController = theAppDelegate.window??.rootViewController as! UINavigationController
        let lastViewController = navigationController.viewControllers.last
        
        if let presentedViewController = lastViewController{
            let alertViewController = UIAlertController.init(title: title, message: message, preferredStyle: style)
//            //Ok Action
//            let okAction = UIAlertAction.init(title: "OK".localized, style: .default) { (action) in
//                complitionHandler(true)
//                alertViewController.dismiss(animated: true, completion: nil)
//            }
//            //Cancel Action
//            let cancelAction = UIAlertAction.init(title: "Cancel".localized, style: .cancel) { (action) in
//                complitionHandler(true)
//                alertViewController.dismiss(animated: true, completion: nil)
//            }
            
           // alertViewController.addAction(okAction)
            //alertViewController.addAction(cancelAction)
            presentedViewController.present(alertViewController, animated: true, completion: {
                
            })
        }
    }
    
    static func showAlertAtTopOfWindowWithMessage(message : String){
        
        let alertcontroller : UIAlertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        let alertActionOkay = UIAlertAction(title: "", style: .default, handler: {
            action in
        })
        alertActionOkay.setValue(UIColor.black, forKey: "titleTextColor")
        alertcontroller.addAction(alertActionOkay)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.present(alertcontroller, animated: true, completion: nil)
    }
    
    static func getScaledImageFrom(image:UIImage, withScalingSize size:CGSize?) -> UIImage? {
        
        var finalImage = image
        if let finalSize = size {
            UIGraphicsBeginImageContext(finalSize)
            image.draw(in: CGRect(x: 0, y: 0, width: finalSize.width, height: finalSize.height))
            finalImage = UIGraphicsGetImageFromCurrentImageContext() ?? image
            UIGraphicsEndImageContext()
        }
        let imageData:Data = UIImagePNGRepresentation(finalImage)!
        let scaledImage = UIImage(data:imageData)
        return scaledImage
    }
}
