//
//  Constant.swift
//  CityOnClicks
//
//  Created by Yash Agarwal on 13/11/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import Foundation

class Constant: NSObject{
    
    struct CCText{
        
        struct Button {
            
            static let ok           = "OK"
            static let cancel       = "Cancel"
            static let skip         = "Skip"
            static let signIn       = "Sign In"
            static let signUp       = "Sign Up"
            static let getAccess    = "Get Access"
            static let forgotPass   = "Forgot Password"
            static let facebook     = "Facebook"
            static let twitter      = "Twitter"
            static let send         = "Send"
            static let referralCode = "Referral Code"
            static let male         = "Male"
            static let female       = "Female"
        }
        
        struct Heading {
            
            static let login  = "Login"
            static let signup = "Signup"
        }
        
        struct Label {
            
            static let or      = "OR"
            static let address = "Address"
            static let gender  = "Gender"
        }
        
        struct Placeholder {
            
            static let emailMob   = "Email/Mobile Number"
            static let password   = "Password"
            static let fullName   = "Full Name"
            static let emailAdd   = "Email Address"
            static let dob        = "Date of Birth"
            static let city       = "City"
            static let postalCode = "Postal Code"
            static let mobileNum  = "Mobile Number"
        }
        
        struct Message{
            
            static let validEmailNo    = "Please enter valid email or phone"
            static let validPass       = "Please enter valid password"
            static let validCredential = "E- mail/Password is incorrect"
            static let forgotPassMsg   = "Please enter your email address and we will send you a link to reset"
        }
    }
    
    // MARK: - Date Formats
    
    static let date_format_1 = "yyyy-MM-dd"
}
