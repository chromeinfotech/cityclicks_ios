//
//  Color.swift
//  CityOnClicks
//
//  Created by Yash Agarwal on 13/11/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    
    static func themeColor() -> UIColor{
        return UIColor(red: 50.0/255.0, green: 79.0/255.0, blue: 161.0/255.0, alpha: 1)
    }
}
