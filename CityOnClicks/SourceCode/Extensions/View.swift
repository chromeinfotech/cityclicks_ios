//
//  View.swift
//  CityOnClicks
//
//  Created by Yash Agarwal on 13/11/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    
    func bottomShadow(_ cornerRadius:Int) {
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.5
    }
    
    func bottomShadow(){
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 2
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 2)
    }
    
    func displayPictureLayoutCircle(){
        self.layer.cornerRadius = self.frame.size.width/2
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 3
        self.clipsToBounds = true
        
    }
    
    func displayPictureLayout(_ view:UIView){
        if view.frame.size.width > 375{
            self.layer.cornerRadius = 120/2
        }else{
            self.layer.cornerRadius = self.frame.size.width/2
        }
        
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 3
        self.clipsToBounds = true
        
    }
    
    func addGredientColor(){
        
        let gradient = CAGradientLayer()
        
        let startColor = UIColor.init(red: 251/255, green: 170/255, blue: 29/255, alpha: 1)
        let endColor = UIColor.init(red: 164/255, green: 76/255, blue: 255/255, alpha: 1)
        
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        gradient.startPoint = CGPoint.init(x: 0, y: 0.5)
        gradient.endPoint = CGPoint.init(x: 1, y: 0.5)
        self.layer.addSublayer(gradient)
        
    }
    
    func addLayerGradientColor(){
        
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradient.colors = [UIColor.black.cgColor, UIColor.white.cgColor]
        
        let shape = CAShapeLayer()
        shape.lineWidth = 4
        shape.path = UIBezierPath(roundedRect: self.bounds,
                                  byRoundingCorners: [.bottomLeft, .bottomRight, .topLeft, .topRight],
                                  cornerRadii: CGSize(width: self.frame.size.width/2, height: 0.0)).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        self.layer.addSublayer(gradient)
    }
    
    func addMask(){
        let shape = CAShapeLayer()
        shape.frame = self.bounds
        shape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 5, height: 5)).cgPath
        self.layer.mask = shape
    }
    
    func removeGredientColor(){
        
        if self.layer.sublayers![0].name == "gradient"{
            self.layer.sublayers![0].removeFromSuperlayer()
        }
    }
}
