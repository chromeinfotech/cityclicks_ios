//
//  Validator.swift
//  MiskTeacher
//
//  Created by Sumit Kumar Gupta on 05/02/18.
//  Copyright © 2018 Sumit Kumar Gupta. All rights reserved.
//

import UIKit

class Validator: NSObject {

    static func isTextFieldEmpty (_ textfield: UITextField) -> Bool{
        
        guard let trimmedText = textfield.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {return false}
        textfield.text = trimmedText
        return (trimmedText.isEmpty)
    }
    
    static func isTextViewEmpty (_ textView: UITextView) -> Bool{
        guard let trimmedText = textView.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {return false}
        textView.text = trimmedText
        return (trimmedText.isEmpty)
    }
    
    static func isValidEmail (_ textfield: UITextField) -> Bool{
        
        guard let email = textfield.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {return false}
        textfield.text = email
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate.init(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: email)
    }
    
    static func isValidDate (_ textfield: UITextField) -> Bool{
        
        guard let date = textfield.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {return false
        }
        textfield.text = date
        let dateRegex = "[0-9]{2}+\\ [A-Za-z]{3}+\\,[0-9]{4}"
        let dateTest = NSPredicate.init(format: "SELF MATCHES %@", dateRegex)
        return dateTest.evaluate(with: date)
    }
    
    static func isValidEmail (_ email: String?) -> Bool{
        guard let email = email?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {return false}
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,4}"
//        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,4}+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate.init(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: email)
    }
    
    static func isValidPassword (_ password: String?) -> Bool{
        guard let password = password?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {return false}
        return password.count > 6 ? true : false
    }
    
    static func isAlphaNumericPassword (_ textfield: UITextField) -> Bool{
        
        guard let trimmedText = textfield.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {return false}
        textfield.text = trimmedText
        let passwordRegex = "^(?!.*(.)\\1{3})((?=.*[\\d])(?=.*[A-Za-z])|(?=.*[^\\w\\d\\s])(?=.*[A-Za-z])).{6,20}$"
        let passwordTest = NSPredicate.init(format: "SELF MATCHES %@", passwordRegex)
        return passwordTest.evaluate(with: trimmedText)
    }
    
    static func isAlphaNumericPassword (_ password: String) -> Bool{
        let passwordRegex = "^(?!.*(.)\\1{3})((?=.*[\\d])(?=.*[A-Za-z])|(?=.*[^\\w\\d\\s])(?=.*[A-Za-z])).{6,20}$"
        let passwordTest = NSPredicate.init(format: "SELF MATCHES %@", passwordRegex)
        return passwordTest.evaluate(with: password)
    }
}
