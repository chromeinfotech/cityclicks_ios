//
//  CCVideoViewController.swift
//  CityOnClicks
//
//  Created by Yash Agarwal on 13/11/18.
//  Copyright © 2018 Yash Agarwal. All rights reserved.
//

import UIKit

class CCVideoViewController: UIViewController {

    // MARK: - Outlets
    
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    
    // MARK: - Override Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
    }
    
    // MARK: - Private Methods
    
    /**
     Customize UI
     */
    private func customizeUI(){
        
        signInButton.bottomShadow(5)
        signUpButton.bottomShadow(5)
        
        signInButton.setTitle(Constant.CCText.Button.signIn, for: .normal)
        signUpButton.setTitle(Constant.CCText.Button.signUp, for: .normal)
        skipButton.setTitle(Constant.CCText.Button.skip, for: .normal)
    }
    
    // MARK: - Actions
    
    @IBAction func signInButton(_ sender: UIButton) {
        let loginController = CCLoginViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(loginController, animated: true)
    }
    
    @IBAction func signUpButton(_ sender: UIButton) {
        let signUpCpntroller = CCSignUpViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(signUpCpntroller, animated: true)
    }
    
    @IBAction func skipButton(_ sender: UIButton) {
    }
}

// MARK: - Class Methods

extension CCVideoViewController{
    /**
     Instantiate Video Controller from Main storyboard
     
     - Returns: CCVideoViewController
     */
    class func instantiateFromStoryboard() -> CCVideoViewController{
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let videoController = storyboard.instantiateViewController(withIdentifier: "CCVideoViewController")
        return videoController as! CCVideoViewController
    }
}
